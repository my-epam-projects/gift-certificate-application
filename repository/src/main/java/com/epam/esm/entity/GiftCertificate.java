package com.epam.esm.entity;

import com.epam.esm.base.BaseEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
/**
 * Represents a gift certificate entity in the system.
 * Extends the {@link BaseEntity} class to inherit id property.
 *
 * @author Jaloliddinov
 */

@Getter
@Setter
@Entity
@Audited
@Table(name = "gift_certificates")
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificate extends BaseEntity {


    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "price", nullable = false)
    private Integer price;

    @Column(name = "duration", nullable = false)
    private Integer duration;

    @Column(name = "create_date")
    private LocalDateTime createDate = LocalDateTime.now();

    @Column(name = "last_update_date")
    private LocalDateTime lastUpdateDate = LocalDateTime.now();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "gift_certificate_tags",
            joinColumns = @JoinColumn(name = "gift_certificate_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags = new HashSet<>();
}
