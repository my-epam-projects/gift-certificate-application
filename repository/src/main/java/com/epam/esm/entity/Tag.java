package com.epam.esm.entity;

import com.epam.esm.base.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a tag entity in the system.
 * Extends the {@link BaseEntity} class to inherit ID property.
 *
 * @author Jaloliddinov
 */

@Getter
@Setter
@Entity
@Audited
@Table(name = "tags")
@AllArgsConstructor
@NoArgsConstructor
public class Tag extends BaseEntity {

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @ManyToMany(mappedBy = "tags")
    private Set<GiftCertificate> giftCertificates = new HashSet<>();

}
