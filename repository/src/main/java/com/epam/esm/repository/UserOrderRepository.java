package com.epam.esm.repository;

import com.epam.esm.entity.UserOrder;
import com.epam.esm.utils.PageRequest;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * Repository class for managing operations related to user orders.
 * Handles CRUD operations, searching, and pagination.
 *
 * @author Jaloliddinov
 */
@Repository
public class UserOrderRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public UserOrder findById(Long id) {
        return entityManager.find(UserOrder.class, id);
    }

    public PageRequest<List<UserOrder>> findByUser(Long userId, Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserOrder> criteriaQuery = criteriaBuilder.createQuery(UserOrder.class);
        Root<UserOrder> root = criteriaQuery.from(UserOrder.class);

        // Select
        criteriaQuery.select(root);

        // Where clause
        Predicate userPredicate = criteriaBuilder.equal(root.get("user").get("id"), userId);
        Predicate filterPredicate = buildFilterPredicate(criteriaBuilder, root, filterField, filterValue);
        if (filterPredicate != null) {
            criteriaQuery.where(criteriaBuilder.and(userPredicate, filterPredicate));
        } else {
            criteriaQuery.where(userPredicate);
        }

        if (sortField != null && !sortField.isEmpty()) {
            Order order = sortOrder.equalsIgnoreCase("desc") ?
                    criteriaBuilder.desc(root.get(sortField)) :
                    criteriaBuilder.asc(root.get(sortField));
            criteriaQuery.orderBy(order);
        }

        TypedQuery<UserOrder> typedQuery = entityManager.createQuery(criteriaQuery);

        // Apply pagination
        applyPagination(typedQuery, page, size);

        return new PageRequest<>(typedQuery.getResultList().size(), typedQuery.getResultList());
    }

    public Integer getHighestCost() {
        final String query = "SELECT MAX(t.cost) FROM UserOrder t";
        TypedQuery<Integer> highestCost = entityManager.createQuery(query, Integer.class);
        return highestCost.getSingleResult();
    }

    public List<UserOrder> finByUser(Long userId) {
        final String query = "SELECT t FROM UserOrder t WHERE t.user.id = :userId";
        TypedQuery<UserOrder> typedQuery = entityManager.createQuery(query, UserOrder.class);
        typedQuery.setParameter("userId", userId);
        return typedQuery.getResultList();
    }

    public void save(UserOrder order) {
        entityManager.persist(order);
    }


    private Predicate buildFilterPredicate(CriteriaBuilder criteriaBuilder, Root<UserOrder> root, String filterField, String filterValue) {
        if (isValidFilter(filterField, filterValue)) {
            return criteriaBuilder.equal(root.get(filterField), filterValue);
        }
        return null;
    }

    private boolean isValidFilter(String filterField, String filterValue) {
        return filterField != null && filterValue != null && !filterField.isEmpty() && !filterValue.isEmpty();
    }

    private void applyPagination(TypedQuery<?> typedQuery, Integer page, Integer size) {
        if (page != null && size != null) {
            typedQuery.setFirstResult((page - 1) * size);
            typedQuery.setMaxResults(size);
        }
    }
}
