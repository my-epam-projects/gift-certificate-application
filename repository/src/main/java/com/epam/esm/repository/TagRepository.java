package com.epam.esm.repository;


import com.epam.esm.entity.Tag;
import com.epam.esm.utils.PageRequest;
import jakarta.persistence.*;
import jakarta.persistence.criteria.*;
import org.springframework.stereotype.Repository;

import java.util.*;
/**
 * Repository class for managing operations related to tags.
 * Handles CRUD operations, searching, and pagination.
 *
 * @author Jaloliddinov
 */
@Repository
public class TagRepository {

    @PersistenceContext
    private EntityManager entityManager;


    public PageRequest<List<Tag>> findAll(Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> criteriaQuery = criteriaBuilder.createQuery(Tag.class);
        Root<Tag> root = criteriaQuery.from(Tag.class);

        // Select
        criteriaQuery.select(root);

        // Where clause
        Predicate filterPredicate = buildFilterPredicate(criteriaBuilder, root, filterField, filterValue);
        if (filterPredicate != null) {
            criteriaQuery.where(filterPredicate);
        }

        if (sortField != null && !sortField.isEmpty()) {
            Order order = sortOrder.equalsIgnoreCase("desc") ?
                    criteriaBuilder.desc(root.get(sortField)) :
                    criteriaBuilder.asc(root.get(sortField));
            criteriaQuery.orderBy(order);
        }

        TypedQuery<Tag> typedQuery = entityManager.createQuery(criteriaQuery);

        // Apply pagination
        applyPagination(typedQuery, page, size);

        return new PageRequest<>(typedQuery.getResultList().size(), typedQuery.getResultList());
    }

    public Tag finById(Long id) {
        return entityManager.find(Tag.class, id);
    }

    public List<Tuple> findMostWidelyUsedTag(Long userId) {
        final String sql = "WITH UserTagCosts AS (SELECT u.id         AS user_id, " +
                "                             t.id         as tag_id, " +
                "                             t.name       AS tag_name, " +
                "                             COUNT(*)     AS tag_count, " +
                "                             SUM(uo.cost) AS total_cost " +
                "                      FROM users u " +
                "                               JOIN user_orders uo ON u.id = uo.user_id " +
                "                               JOIN gift_certificates gc ON uo.gift_certificate_id = gc.id " +
                "                               JOIN gift_certificate_tags gct ON gc.id = gct.gift_certificate_id " +
                "                               JOIN tags t ON gct.tag_id = t.id " +
                "                      WHERE u.id = :user_id " +
                "                      GROUP BY u.id, t.name,t.id) " +
                "SELECT user_id, " +
                "       tag_id, " +
                "       tag_name, " +
                "       tag_count, " +
                "       total_cost " +
                "FROM UserTagCosts " +
                "WHERE (tag_count, total_cost) = (SELECT MAX(tag_count), " +
                "                                        MAX(total_cost) " +
                "                                 FROM UserTagCosts);";


        return (List<Tuple>) new ArrayList<>(entityManager.createNativeQuery(sql, Tuple.class).setParameter("user_id", userId).getResultList());
    }


    public void save(Tag tag) {
        entityManager.persist(tag);
    }

    public void update(Tag tag) {
        entityManager.merge(tag);
    }

    public void delete(Tag tag) {
        entityManager.remove(tag);
    }

    public Map<Long, Boolean> areIdsValid(List<Long> ids) {
        Map<Long, Boolean> idValidityMap = new HashMap<>();


        final String jpql = "SELECT e.id FROM Tag e WHERE e.id IN :ids";
        TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        query.setParameter("ids", ids);

        List<Long> foundIds = query.getResultList();

        for (Long id : ids) {
            idValidityMap.put(id, foundIds.contains(id));
        }

        return idValidityMap;
    }

    public List<Tag> findByIds(List<Long> ids) {
        final String query = "SELECT t FROM Tag t WHERE t.id IN :ids";
        TypedQuery<Tag> typedQuery = entityManager.createQuery(query, Tag.class);
        typedQuery.setParameter("ids", ids);
        return typedQuery.getResultList();
    }

    public Tag finByName(String name) {
        final String query = "SELECT t FROM Tag t WHERE t.name = :name";
        try {
            return entityManager.createQuery(query, Tag.class).setParameter("name", name).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    private Predicate buildFilterPredicate(CriteriaBuilder criteriaBuilder, Root<Tag> root, String filterField, String filterValue) {
        if (isValidFilter(filterField, filterValue)) {
            return criteriaBuilder.equal(root.get(filterField), filterValue);
        }
        return null;
    }

    private boolean isValidFilter(String filterField, String filterValue) {
        return filterField != null && filterValue != null && !filterField.isEmpty() && !filterValue.isEmpty();
    }

    private void applyPagination(TypedQuery<?> typedQuery, Integer page, Integer size) {
        if (page != null && size != null) {
            typedQuery.setFirstResult((page - 1) * size);
            typedQuery.setMaxResults(size);
        }
    }

//    public void generateAndSave() {
//        Faker faker = new Faker();
//        for (int i = 0; i < 1000; i++) {
//            Tag tag = new Tag();
//            tag.setName(faker.lorem().word() + i);
//            entityManager.persist(tag);
//        }
//    }

    public Set<Tag> getRandomTagsWithRandomCount() {
        return new HashSet<>(entityManager.createNativeQuery(
                        "SELECT * FROM tags ORDER BY RANDOM() LIMIT CAST(FLOOR(RANDOM() * 10) + 1 AS INTEGER)",
                        Tag.class)
                .getResultList());
    }
}
