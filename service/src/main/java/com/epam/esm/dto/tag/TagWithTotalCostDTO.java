package com.epam.esm.dto.tag;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TagWithTotalCostDTO {

    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("tag_id")
    private Long tagId;

    @JsonProperty("name")
    private String name;

    @JsonProperty(namespace = "tag_count")
    private Long tagCount;

    @JsonProperty("total_cost")
    private Long totalCost;
}
