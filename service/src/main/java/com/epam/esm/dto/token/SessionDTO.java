package com.epam.esm.dto.token;

import com.epam.esm.dto.user.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SessionDTO {


    private String accessToken;

    private String refreshToken;

    private UserDTO user;
}
