package com.epam.esm.dto.custom;

import com.epam.esm.dto.tag.TagDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomDTO {

    @JsonProperty("the most widely used Tag")
    private TagDTO tag;

    @JsonProperty("Highest cost of All orders")
    private Integer cost;
}
