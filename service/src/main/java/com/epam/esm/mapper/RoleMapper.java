package com.epam.esm.mapper;

import com.epam.esm.dto.role.RoleDTO;
import com.epam.esm.entity.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Mapper(componentModel = "spring")
public interface RoleMapper {

    @Mapping(target = "id",source = "id")
    @Mapping(target = "name",source = "name")
    @Mapping(target = "description",source = "description")
    RoleDTO toDto(Role role);

    List<RoleDTO> toDtoList(List<Role> roleList);
}
