package com.epam.esm.mapper;

import com.epam.esm.dto.tag.TagCreateDTO;
import com.epam.esm.dto.tag.TagDTO;
import com.epam.esm.dto.tag.TagUpdateDTO;
import com.epam.esm.entity.Tag;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Mapper(componentModel = "spring")
public interface TagMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    TagDTO toDto(Tag tag);

    List<TagDTO> toDtoList(List<Tag> tagList);

    @Mapping(target = "name", source = "dto.name")
    Tag toEntity(TagCreateDTO dto);

    @Mapping(target = "name", source = "dto.name")
    Tag toEntity(TagUpdateDTO dto);
}
