package com.epam.esm.mapper;

import com.epam.esm.dto.user.UserCreateDTO;
import com.epam.esm.dto.user.UserDTO;
import com.epam.esm.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Mapper(componentModel = "spring", uses = RoleMapper.class)
public interface UserMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "username", source = "username")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "roles", source = "roles")
    UserDTO toDto(User user);

    List<UserDTO> toDtoList(List<User> userList);

    @Mapping(target = "username", source = "username")
    @Mapping(target = "email", source = "email")
    User toEntity(UserCreateDTO dto);
}
