package com.epam.esm.mapper;

import com.epam.esm.dto.user_order.UserOrderDTO;
import com.epam.esm.entity.UserOrder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Mapper(componentModel = "spring", uses = {GiftCertificateMapper.class, UserMapper.class})
public interface UserOrderMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "certificate", source = "giftCertificate")
    @Mapping(target = "cost", source = "cost")
    @Mapping(target = "purchaseTime", source = "purchaseTime")
    UserOrderDTO toDto(UserOrder userOrder);

    List<UserOrderDTO> toDtoList(List<UserOrder> orderList);

}
