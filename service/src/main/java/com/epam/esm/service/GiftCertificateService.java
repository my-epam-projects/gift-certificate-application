package com.epam.esm.service;

import com.epam.esm.dto.gift_certificate.GiftCertificateCreateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateUpdateDTO;
import com.epam.esm.dto.gift_certificate_tag.GiftCertificateRequestDTO;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.utils.PageRequest;

import java.util.List;
import java.util.Set;
/**
 * Service interface for managing gift certificates.
 * Defines methods for CRUD operations, searching, and pagination.
 *
 * @author Jaloliddinov
 */
public interface GiftCertificateService {


    /**
     * Retrieves a gift certificate by its ID.
     *
     * @param id The ID of the gift certificate.
     * @return The gift certificate with the specified ID.
     */
    GiftCertificateDTO getById(Long id);

    /**
     * Creates a new gift certificate based on the provided DTO.
     *
     * @param dto The DTO containing information for creating the gift certificate.
     * @return The created gift certificate.
     */
    GiftCertificateDTO create(GiftCertificateCreateDTO dto);

    /**
     * Updates an existing gift certificate based on the provided ID and DTO.
     *
     * @param id  The ID of the gift certificate to update.
     * @param dto The DTO containing information for updating the gift certificate.
     * @return The updated gift certificate.
     */
    GiftCertificateDTO update(Long id, GiftCertificateUpdateDTO dto);

    /**
     * Updates only the price of an existing gift certificate.
     *
     * @param id    The ID of the gift certificate to update.
     * @param price The new price for the gift certificate.
     * @return The updated gift certificate.
     */
    GiftCertificateDTO updateOnlyPrice(Long id, Integer price);

    /**
     * Deletes a gift certificate based on its ID.
     *
     * @param id The ID of the gift certificate to delete.
     * @return True if the deletion was successful, false otherwise.
     */
    Boolean delete(Long id);

    /**
     * Retrieves a paginated list of gift certificates filtered by name, with optional filtering and sorting.
     *
     * @param name        Name to filter the result by.
     * @param page        Page number to retrieve.
     * @param size        Number of items per page.
     * @param sortField   Field to sort the result by.
     * @param sortOrder   Sort order (asc/desc).
     * @param filterField Field to filter the result by.
     * @param filterValue Value to filter the result by.
     * @return A paginated list of gift certificates filtered by name.
     */
    PageRequest<List<GiftCertificateDTO>> getAllByName(String name, Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue);

    /**
     * Retrieves a gift certificate by its ID.
     *
     * @param id The ID of the gift certificate.
     * @return The gift certificate with the specified ID.
     */
    GiftCertificate findById(Long id);

    /**
     * Retrieves a paginated list of all gift certificates, with optional filtering and sorting.
     *
     * @param page        Page number to retrieve.
     * @param size        Number of items per page.
     * @param sortField   Field to sort the result by.
     * @param sortOrder   Sort order (asc/desc).
     * @param filterField Field to filter the result by.
     * @param filterValue Value to filter the result by.
     * @return A paginated list of all gift certificates.
     */
    PageRequest<List<GiftCertificateDTO>> getAll(Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue);

    /**
     * Retrieves a paginated list of gift certificates filtered by tag names, with optional filtering and sorting.
     *
     * @param tagNameList Set of tag names to filter by.
     * @param page        Page number to retrieve.
     * @param size        Number of items per page.
     * @param sortField   Field to sort the result by.
     * @param sortOrder   Sort order (asc/desc).
     * @param filterField Field to filter the result by.
     * @param filterValue Value to filter the result by.
     * @return A paginated list of gift certificates filtered by tag names.
     */
    PageRequest<List<GiftCertificateDTO>> getByTags(Set<String> tagNameList, Integer page, Integer size, String sortField, String sortOrder, String filterField, String filterValue);

    /**
     * Adds new tags to a gift certificate based on the provided DTO.
     *
     * @param dto The DTO containing information for adding tags to the gift certificate.
     * @return The updated gift certificate with the new tags.
     */
    GiftCertificateDTO addNewTags(GiftCertificateRequestDTO dto);

    /**
     * Deletes tags from a gift certificate based on the provided certificate ID and tag IDs.
     *
     * @param certificateId The ID of the gift certificate from which to delete tags.
     * @param tags          Set of tag IDs to delete from the gift certificate.
     * @return True if the deletion was successful, false otherwise.
     */
    Boolean deleteTags(Long certificateId, Set<Long> tags);

    /**
     * Generates and saves a set of gift certificates with random data.
     */
//    void generateAndSave();

    /**
     * Retrieves a random gift certificate.
     *
     * @return A randomly selected gift certificate.
     */
    GiftCertificate getRandomCertificate();

}
