package com.epam.esm.service.impl;

import com.epam.esm.entity.Role;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.helper.Utils;
import com.epam.esm.repository.RoleRepository;
import com.epam.esm.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public Role getById(Long id) {
        Role role = roleRepository.findById(id);
        if (Utils.isEmpty(role)) {
            throw new CustomNotFoundException("Role with ID: " + id + " is not found");
        }
        return role;
    }
}
