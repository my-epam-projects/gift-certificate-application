package com.epam.esm.controller;


import com.epam.esm.base.BaseURI;
import com.epam.esm.common.ResponseData;
import com.epam.esm.dto.gift_certificate.GiftCertificateCreateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateUpdateDTO;
import com.epam.esm.dto.gift_certificate_tag.GiftCertificateRequestDTO;
import com.epam.esm.hateoes.HateoasAssembler;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.utils.PageRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping(BaseURI.API + BaseURI.V1 + BaseURI.GIFT_CERTIFICATES)
public class GiftCertificateController {

    private final GiftCertificateService certificateService;
    private final HateoasAssembler<GiftCertificateDTO> hateoasAssembler;


    @GetMapping
    public ResponseEntity<ResponseData<PageRequest<List<GiftCertificateDTO>>>> getAll(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,
            @RequestParam(value = "sort", defaultValue = "id") String sortField,
            @RequestParam(value = "order", defaultValue = "asc") String sortOrder,
            @RequestParam(value = "filter", required = false) String filterField,
            @RequestParam(value = "value", required = false) String filterValue) {
        PageRequest<List<GiftCertificateDTO>> pageRequest = certificateService.getAll(page, size, sortField, sortOrder, filterField, filterValue);
        hateoasAssembler.addLinks(pageRequest.getResultList());
        return ResponseData.success200(pageRequest);
    }


    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<GiftCertificateDTO>> getById(@PathVariable(value = "id") Long id) {
        GiftCertificateDTO dto = certificateService.getById(id);
        hateoasAssembler.addLinks(dto);
        return ResponseData.success200(dto);
    }


    @GetMapping(BaseURI.BY_NAME)
    public ResponseEntity<ResponseData<PageRequest<List<GiftCertificateDTO>>>> getByName(@RequestParam(value = "name") String name, @RequestParam(value = "page", defaultValue = "1") Integer page,
                                                                                         @RequestParam(value = "size", defaultValue = "10") Integer size,
                                                                                         @RequestParam(value = "sort", defaultValue = "id") String sortField,
                                                                                         @RequestParam(value = "order", defaultValue = "asc") String sortOrder,
                                                                                         @RequestParam(value = "filter", required = false) String filterField,
                                                                                         @RequestParam(value = "value", required = false) String filterValue) {
        PageRequest<List<GiftCertificateDTO>> pageRequest = certificateService.getAllByName(name, page, size, sortField, sortOrder, filterField, filterValue);
        hateoasAssembler.addLinks(pageRequest.getResultList());
        return ResponseData.success200(pageRequest);
    }

    @GetMapping(BaseURI.SEARCH_BY_TAGS)
    public ResponseEntity<ResponseData<PageRequest<List<GiftCertificateDTO>>>> getByTags(@RequestParam(value = "tagNames") Set<String> tagNameList,
                                                                                         @RequestParam(value = "page", defaultValue = "1") Integer page,
                                                                                         @RequestParam(value = "size", defaultValue = "10") Integer size,
                                                                                         @RequestParam(value = "sort", defaultValue = "id") String sortField,
                                                                                         @RequestParam(value = "order", defaultValue = "asc") String sortOrder,
                                                                                         @RequestParam(value = "filter", required = false) String filterField,
                                                                                         @RequestParam(value = "value", required = false) String filterValue) {
        PageRequest<List<GiftCertificateDTO>> pageRequest = certificateService.getByTags(tagNameList, page, size, sortField, sortOrder, filterField, filterValue);
        hateoasAssembler.addLinks(pageRequest.getResultList());
        return ResponseData.success200(pageRequest);
    }


    @Operation(summary = "Create Gift Certificate", description = "Create Gift Certificate")
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity<ResponseData<GiftCertificateDTO>> create(@Valid @RequestBody GiftCertificateCreateDTO dto) {
        GiftCertificateDTO certificate = certificateService.create(dto);
        hateoasAssembler.addLinks(certificate);
        return ResponseData.success201(certificate);
    }


    @Operation(summary = "Update Gift Certificate", description = "Update Gift Certificate")
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<GiftCertificateDTO>> update(@PathVariable(value = "id") Long id,
                                                                   @Valid @RequestBody GiftCertificateUpdateDTO dto) {
        GiftCertificateDTO certificate = certificateService.update(id, dto);
        hateoasAssembler.addLinks(certificate);
        return ResponseData.success202(certificate);
    }


    @Operation(summary = "Update a price of Gift Certificate", description = "Update a price of Gift Certificate")
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasRole('ADMIN')")
    @PatchMapping("/{id}")
    public ResponseEntity<ResponseData<GiftCertificateDTO>> updateOnlyPrice(@PathVariable(value = "id") Long id,
                                                                            @RequestParam(value = "price") Integer price) {
        GiftCertificateDTO dto = certificateService.updateOnlyPrice(id, price);
        hateoasAssembler.addLinks(dto);
        return ResponseData.success202(dto);
    }


    @Operation(summary = "Delete Gift Certificate", description = "Delete Gift Certificate")
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseData<Boolean>> delete(@PathVariable(value = "id") Long id) {
        return ResponseData.success204(certificateService.delete(id));
    }


    @Operation(summary = "Add new tag", description = "Add new tag to gift certificate")
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/add_tags")
    public ResponseEntity<ResponseData<GiftCertificateDTO>> addNewTag(@Valid @RequestBody GiftCertificateRequestDTO dto) {
        GiftCertificateDTO certificateDTO = certificateService.addNewTags(dto);
        hateoasAssembler.addLinks(certificateDTO);
        return ResponseData.success201(certificateDTO);
    }


    @Operation(summary = "Delete tags", description = "Delete tags")
    @SecurityRequirement(name = "Bearer Authentication")
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete_tags/{id}")
    public ResponseEntity<ResponseData<Boolean>> deleteTags(@PathVariable(value = "id") Long certificateId,
                                                            @RequestParam(value = "tags") Set<Long> tags) {
        return ResponseData.success204(certificateService.deleteTags(certificateId, tags));
    }
}
