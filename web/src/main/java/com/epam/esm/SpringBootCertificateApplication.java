package com.epam.esm;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.hateoas.config.EnableHypermediaSupport;

@SpringBootApplication
@RequiredArgsConstructor
@OpenAPIDefinition(info = @Info(title = "GIFT CERTIFICATE", description = "GIFT CERTIFICATE APIs"))
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
@ComponentScan(basePackages = {"com.epam.esm","com.epam.esm.mapper"})
public class SpringBootCertificateApplication {


    public static void main(String[] args) {
        SpringApplication.run(SpringBootCertificateApplication.class, args);
    }


}
